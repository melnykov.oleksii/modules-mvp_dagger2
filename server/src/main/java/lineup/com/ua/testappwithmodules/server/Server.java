package lineup.com.ua.testappwithmodules.server;

import android.os.Handler;

import org.jetbrains.annotations.Nullable;

import java.util.Timer;
import java.util.TimerTask;

/**
 * <p> Created by Melnykov Oleksii on 2/21/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.core </p>
 * Репозиторий, эмулирует работу сервера
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public class Server implements ServerContract {

    @Nullable private Timer mTimer;
    @Nullable private Handler mHandler;
    @Nullable private Callback mCallback;
    private boolean isServerStarted = false;

    @Override
    public void setServerCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void startServer() {
        isServerStarted = true;
        mTimer = new Timer();
        mHandler = new Handler();

        final int[] count = { 0 };
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(() -> {
                    if (mCallback != null) mCallback.sendMessage("Hello from server " + count[0]);
                    count[0]++;
                });
            }
        }, 0, 1000);
    }

    @Override
    public void stopServer() {
        if (!isServerStarted || mTimer == null) {
            if (mCallback != null) mCallback.sendMessage("Already stopped!");
            return;
        }

        mTimer.cancel();
        mTimer = null;
        if (mCallback != null) mCallback.sendMessage("Stopped");
        isServerStarted = false;
    }

}