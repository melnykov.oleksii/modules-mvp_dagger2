package lineup.com.ua.testappwithmodules.server;

/**
 * <p> Created by Melnykov Oleksii on 2/25/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.core.contract </p>
 * Интерфейс, который описывает возможности Server
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public interface ServerContract {
    /**
     * Запустить сервер
     */
    void startServer();

    /**
     * Остановить сервер
     */
    void stopServer();

    /**
     * Установка слушателя исходящих сообщений из Server
     */
    void setServerCallback(Callback callback);

    /**
     * Интерфейс, который описывает исходящиие сообщения из Server
     */
    interface Callback {
        /**
         * Передать сообщение от сервера
         */
        void sendMessage(String msg);
    }

}