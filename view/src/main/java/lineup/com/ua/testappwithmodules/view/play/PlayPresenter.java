package lineup.com.ua.testappwithmodules.view.play;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import lineup.com.ua.testappwithmodules.core.DataContract;
/**
 * <p> Created by Melnykov Oleksii on 2/22/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.gps.play </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public class PlayPresenter implements PlayContract.Presenter, DataContract.Callback {

    @Nullable private PlayContract.View mView;
    private final DataContract mDataContract;

    @Inject
    public PlayPresenter(DataContract dataContract) {
        mDataContract = dataContract;
        mDataContract.setDataCallback(this);
    }

    @Override
    public void takeView(@NonNull PlayContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void stop() {
        mDataContract.stopData();
    }

    @Override
    public void sendMessage(String msg) {
        if (mView != null) mView.showText(msg);
    }
}