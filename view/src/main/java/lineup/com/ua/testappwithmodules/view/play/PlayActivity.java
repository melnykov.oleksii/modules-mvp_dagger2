package lineup.com.ua.testappwithmodules.view.play;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lineup.com.ua.testappwithmodules.view.R;

import static lineup.com.ua.testappwithmodules.view.di.play.PlayInjector.inject;

/**
 * <p> Created by Melnykov Oleksii on 2/22/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.gps </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public class PlayActivity extends Activity implements PlayContract.View {

    @BindView(R.id.activity_gps_msg)
    TextView mTextView;

    @OnClick(R.id.activity_gps_btn)
    void onStopClick() {
        mPresenter.stop();
    }

    @Inject PlayContract.Presenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject(this);
        setContentView(R.layout.activity_gps);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.dropView();
    }

    @Override
    public void showText(String text) {
        mTextView.setText(text);
    }

}