package lineup.com.ua.testappwithmodules.view.play;

import lineup.com.ua.testappwithmodules.view.base.BasePresenter;
import lineup.com.ua.testappwithmodules.view.base.BaseView;

/**
 * <p> Created by Melnykov Oleksii on 2/22/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.gps.play </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public interface PlayContract {

    interface View extends BaseView {
        void showText(String text);
    }

    interface Presenter extends BasePresenter<View> {
        void stop();
    }

}