package lineup.com.ua.testappwithmodules.view.di.play;

import dagger.Component;
import dagger.android.AndroidInjector;
import lineup.com.ua.testappwithmodules.view.play.PlayActivity;
/**
 * <p> Created by Melnykov Oleksii on 2/28/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.view.di.play </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
@Component(modules = PlayModule.class)
public interface PlayComponent extends AndroidInjector<PlayActivity> {

    // Синтаксический сахар
    @Component.Builder
    interface Builder {
        PlayComponent build();
        PlayComponent.Builder playModule(PlayModule playModule);
    }

}