package lineup.com.ua.testappwithmodules.view.di.play;

import lineup.com.ua.testappwithmodules.view.play.PlayActivity;

/**
 * <p> Created by Melnykov Oleksii on 2/28/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.view.di.play </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public class PlayInjector {

    public static void inject(PlayActivity activity) {
        DaggerPlayComponent.builder()
                .playModule(new PlayModule(activity))
                .build()
                .inject(activity);
    }

}