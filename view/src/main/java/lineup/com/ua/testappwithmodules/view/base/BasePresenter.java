package lineup.com.ua.testappwithmodules.view.base;

import androidx.annotation.NonNull;

/**
 * <p> Created by Melnykov Oleksii on 18-Jun-18. <br>
 * Copyright (c) 2018 LineUp. <br>
 * Project: gpstrackerforpost, com.lineup.forpost.tracker.gpstrackerforpost.base </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public interface BasePresenter<T> {
    void takeView(@NonNull T view);
    void dropView();
}
