package lineup.com.ua.testappwithmodules.view.di.play;

import android.app.Activity;
import android.content.Context;

import java.util.Objects;

import dagger.Module;
import dagger.Provides;
import lineup.com.ua.testappwithmodules.App;
import lineup.com.ua.testappwithmodules.core.DataContract;
import lineup.com.ua.testappwithmodules.view.play.PlayContract;
import lineup.com.ua.testappwithmodules.view.play.PlayPresenter;
/**
 * <p> Created by Melnykov Oleksii on 2/28/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.view.di.play </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
@Module
@SuppressWarnings("unused")
class PlayModule {
    private final Activity mActivity;
    private final DataContract mDataContract;

    PlayModule(Activity activity) {
        super();
        mActivity = activity;
        if (activity.getApplication() instanceof App) {
            mDataContract = ((App) activity.getApplication()).getDataContract();
        } else mDataContract = null;

        Objects.requireNonNull(mActivity, "PlayModule Activity");
        Objects.requireNonNull(mDataContract, "PlayModule DataContract");
    }

    @Provides
    Context provideContext() {
        return mActivity;
    }

    @Provides
    PlayContract.Presenter providePlayPresenter() {
        return new PlayPresenter(mDataContract);
    }

}