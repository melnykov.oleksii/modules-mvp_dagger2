package lineup.com.ua.testappwithmodules.core.di;

/**
 * <p> Created by Melnykov Oleksii on 2/21/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.core.di </p>
 * Интерфейс для предоставления компонента модуля, использовать в Application классе
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public interface CoreComponentProvider {
    CoreComponent provideCoreComponent();
}