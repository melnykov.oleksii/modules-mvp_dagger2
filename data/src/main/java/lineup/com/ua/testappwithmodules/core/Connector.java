package lineup.com.ua.testappwithmodules.core;

import androidx.annotation.Nullable;
import lineup.com.ua.testappwithmodules.server.Server;
import lineup.com.ua.testappwithmodules.server.ServerContract;

/**
 * <p> Created by Melnykov Oleksii on 2/22/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.core.contract </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public class Connector implements DataContract, ServerContract.Callback {

    @Nullable private DataContract.Callback mDataCallback;
    private final ServerContract mServer;

    public Connector(Server server) {
        mServer = server;
        mServer.setServerCallback(this);
    }

    @Override
    public void setDataCallback(@Nullable DataContract.Callback callback) {
        mDataCallback = callback;
    }

    //region обработка событий модуля

    @Override
    public void startData() {
        mServer.startServer();
    }

    @Override
    public void stopData() {
        mServer.stopServer();
    }

    //endregion

    //region отправка сообщений с сервера через исходящие сообщения модуля

    @Override
    public void sendMessage(String msg) {
        if (mDataCallback != null) mDataCallback.sendMessage(msg);
    }

    //endregion

}