package lineup.com.ua.testappwithmodules.core.di;

import javax.inject.Singleton;

import dagger.Component;
import lineup.com.ua.testappwithmodules.core.DataContract;

/**
 * <p> Created by Melnykov Oleksii on 2/21/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.core.di </p>
 * Глобальный компонент для модуля Data
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
@Component(modules = {
        CoreModule.class // основной модуль
})
@Singleton
public interface CoreComponent {
    DataContract getDataContract();

    // Синтаксический сахар
    @Component.Builder
    interface Builder {
        CoreComponent build();
    }

}