package lineup.com.ua.testappwithmodules.core;

import androidx.annotation.Nullable;
/**
 * <p> Created by Melnykov Oleksii on 2/22/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.core </p>
 * Интерфейс, который описывает возможности модуля Data
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public interface DataContract {
    /**
     * Запуск сервера / инициализация БД и т.п.
     */
    void startData();

    /**
     * Остановка сервера, закрытие соединения с БД и т.п.
     */
    void stopData();

    /**
     * Установка слушателя исходящих сообщений от модуля Data
     */
    void setDataCallback(@Nullable Callback callback);

    /**
     * Интерфейс, который описывает исходящие события модуля Data
     */
    interface Callback {
        /**
         * Передать сообщение
         */
        void sendMessage(String msg);
    }

}