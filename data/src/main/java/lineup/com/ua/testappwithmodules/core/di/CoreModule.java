package lineup.com.ua.testappwithmodules.core.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import lineup.com.ua.testappwithmodules.core.Connector;
import lineup.com.ua.testappwithmodules.core.DataContract;
import lineup.com.ua.testappwithmodules.server.Server;

/**
 * <p> Created by Melnykov Oleksii on 2/21/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.core.di </p>
 * Основной модуль для Data
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
@Module
class CoreModule {

    @Provides
    @Singleton
    Connector provideConnector() { return new Connector(new Server()); }

    @Provides
    @Singleton
    DataContract provideDataContract(Connector connector) { return connector; }

}