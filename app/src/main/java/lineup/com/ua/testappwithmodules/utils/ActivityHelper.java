package lineup.com.ua.testappwithmodules.utils;

import android.content.Intent;
/**
 * <p> Created by Melnykov Oleksii on 2/28/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules.utils </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public class ActivityHelper {

    static final String PACKAGE_NAME = "lineup.com.ua.testappwithmodules";

    public static Intent intentTo(AddressableActivity addressableActivity) {
        return new Intent(Intent.ACTION_VIEW).setClassName(
                PACKAGE_NAME,
                addressableActivity.className());
    }


}