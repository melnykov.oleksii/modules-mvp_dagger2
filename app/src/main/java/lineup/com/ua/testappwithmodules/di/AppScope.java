package lineup.com.ua.testappwithmodules.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * <p> Created by Melnykov Oleksii on 2/21/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules.di </p>
 * Область видимости - приложение
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
@interface AppScope { }