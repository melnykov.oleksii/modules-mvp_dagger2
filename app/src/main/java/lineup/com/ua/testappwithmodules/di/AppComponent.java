package lineup.com.ua.testappwithmodules.di;

import android.app.Application;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import lineup.com.ua.testappwithmodules.App;
import lineup.com.ua.testappwithmodules.core.di.CoreComponent;

/**
 * <p> Created by Melnykov Oleksii on 2/21/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules.di </p>
 * Компонент приложения
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
@AppScope
@Component(
        // прописывается зависимось от других компонентов (модулей)
        dependencies = {
                CoreComponent.class // модуль data
        },
        // прописываются модули объявляемого компонента
        modules = {
                AndroidSupportInjectionModule.class, // модуль с android компонентами
                AppModule.class // модуль уровня application
        })
public interface AppComponent extends AndroidInjector<App> {

    // Синтаксический сахар
    @Component.Builder
    interface Builder {
        @BindsInstance
        AppComponent.Builder application(Application application);
        AppComponent.Builder coreComponent(CoreComponent coreComponent);
        AppComponent build();
    }

}