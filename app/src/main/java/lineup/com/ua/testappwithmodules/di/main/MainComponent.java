package lineup.com.ua.testappwithmodules.di.main;

import dagger.Component;
import dagger.android.AndroidInjector;
import lineup.com.ua.testappwithmodules.main.MainActivity;
/**
 * <p> Created by Melnykov Oleksii on 2/28/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules.di.main </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
@Component(modules = MainModule.class)
public interface MainComponent extends AndroidInjector<MainActivity> {

    // Синтаксический сахар
    @Component.Builder
    interface Builder {
        MainComponent build();
        MainComponent.Builder mainModule(MainModule mainModule);
    }

}