package lineup.com.ua.testappwithmodules;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import lineup.com.ua.testappwithmodules.core.DataContract;
import lineup.com.ua.testappwithmodules.core.di.CoreComponent;
import lineup.com.ua.testappwithmodules.core.di.CoreComponentProvider;
import lineup.com.ua.testappwithmodules.core.di.DaggerCoreComponent;
import lineup.com.ua.testappwithmodules.di.DaggerAppComponent;

/**
 * <p> Created by Melnykov Oleksii on 2/21/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules </p>
 * Базовый класс приложения
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public class App extends DaggerApplication implements CoreComponentProvider {

    private CoreComponent mCoreComponent;
    @Inject DataContract mDataContract;

    /**
     * Построение компонента Application ({@link lineup.com.ua.testappwithmodules.di.AppComponent})
     */
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder()
                .application(this)
                .coreComponent(provideCoreComponent())
                .build();
    }

    /**
     * Построение компонента Data ({@link lineup.com.ua.testappwithmodules.core.di.CoreComponent})
     */
    @Override
    public CoreComponent provideCoreComponent() {
        if (mCoreComponent == null) mCoreComponent = DaggerCoreComponent.builder().build();
        return mCoreComponent;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mDataContract.startData();
    }

    public DataContract getDataContract() {
        return mDataContract;
    }

}