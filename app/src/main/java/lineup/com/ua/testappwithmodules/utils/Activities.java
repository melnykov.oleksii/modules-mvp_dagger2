package lineup.com.ua.testappwithmodules.utils;

import static lineup.com.ua.testappwithmodules.utils.ActivityHelper.PACKAGE_NAME;
/**
 * <p> Created by Melnykov Oleksii on 2/28/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules.utils </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public class Activities {

    public static class ViewActivity implements AddressableActivity {
        @Override
        public String className() {
            return PACKAGE_NAME + ".view.play.PlayActivity";
        }
    }

}