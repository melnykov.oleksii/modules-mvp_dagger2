package lineup.com.ua.testappwithmodules.utils;

/**
 * <p> Created by Melnykov Oleksii on 2/28/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules.utils </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public interface AddressableActivity {
    String className();
}