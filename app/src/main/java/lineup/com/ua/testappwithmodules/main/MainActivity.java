package lineup.com.ua.testappwithmodules.main;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lineup.com.ua.testappwithmodules.R;
import lineup.com.ua.testappwithmodules.di.main.MainInjector;
import lineup.com.ua.testappwithmodules.utils.Activities;

import static lineup.com.ua.testappwithmodules.utils.ActivityHelper.intentTo;

/**
 * <p> Created by Melnykov Oleksii on 2/21/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules </p>
 * Самое главвное активити
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public class MainActivity extends Activity {

    /**
     * По нажатию на кнопку "Open GPS" запускается activity из модуля gps
     */
    @OnClick(R.id.activity_main_statistic_btn)
    void onOpenGPSClick() {
        startActivity(intentTo(new Activities.ViewActivity()));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainInjector.inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

}