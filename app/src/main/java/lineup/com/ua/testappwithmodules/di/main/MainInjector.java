package lineup.com.ua.testappwithmodules.di.main;

import lineup.com.ua.testappwithmodules.main.MainActivity;
/**
 * <p> Created by Melnykov Oleksii on 2/28/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules.di.main </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
public class MainInjector {

    public static void inject(MainActivity activity) {
        DaggerMainComponent.builder()
                .mainModule(new MainModule(activity))
                .build()
                .inject(activity);
    }

}