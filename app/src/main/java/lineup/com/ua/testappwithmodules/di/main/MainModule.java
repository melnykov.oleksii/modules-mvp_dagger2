package lineup.com.ua.testappwithmodules.di.main;

import android.app.Activity;
import android.content.Context;

import java.util.Objects;

import dagger.Module;
import dagger.Provides;

/**
 * <p> Created by Melnykov Oleksii on 2/28/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules.di.main </p>
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
@Module
@SuppressWarnings("unused")
class MainModule {
    private final Activity mActivity;

    MainModule(Activity activity) {
        super();
        mActivity = activity;

        Objects.requireNonNull(mActivity, "MainModule Activity");
    }

    @Provides
    Context provideContext() {
        return mActivity;
    }

}