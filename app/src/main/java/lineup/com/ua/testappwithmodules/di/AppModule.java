package lineup.com.ua.testappwithmodules.di;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

/**
 * <p> Created by Melnykov Oleksii on 2/21/19. <br>
 * Copyright (c) 2019 LineUp. <br>
 * Project: TestAppWithModules, lineup.com.ua.testappwithmodules.di </p>
 * Модуль - интерфейс, который является самым глобальным в приложении и служит проводником для
 * элементов уровня приложения, к примеру - application context
 *
 * @author Melnykov Oleksii
 * @version 1.0
 */
@Module
@SuppressWarnings("unused")
public interface AppModule {

    @Binds
    @AppScope
    Context bindContext(Application application);

}